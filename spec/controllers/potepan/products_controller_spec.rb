require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe '商品詳細ページ' do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it '200レスポンスが返ってきているか' do
      expect(response).to have_http_status '200'
    end

    it '@productがアサインされる' do
      expect(assigns(:product)).to eq product
    end

    it '商品名が含まれているか' do
      expect(assigns(:product).name).to include product.name
    end

    it '商品説明が含まれているか' do
      expect(assigns(:product).description).to include product.description
    end
  end
end
