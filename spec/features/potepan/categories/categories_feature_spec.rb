require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  feature "カテゴリの商品一覧ページを見ることができる" do
    given!(:taxonomy) { create(:taxonomy, name: 'Categories') }
    given!(:taxon_a)  { create(:taxon, taxonomy: taxonomy, name: 'Categories') }
    given!(:taxon_b)  { create(:taxon, taxonomy: taxonomy, parent_id: taxon_a.id) }
    given!(:product)  { create(:product, taxons: [taxon_b]) }

    background do
      visit potepan_category_path(taxon_b.id)
    end

    scenario "ページ全体で表示されているもの" do
      expect(page).to have_selector 'h2', text: taxon_b.name
      expect(page).to have_link 'Home', href: potepan_index_path
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content 'Categories'
      expect(page).to have_content taxon_b.name
      expect(page).to have_content taxon_b.products.count
    end

    scenario "商品をクリックすると商品の詳細ページに移動する" do
      expect(page).to have_link product.name
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    scenario "サイドバーの挙動の確認" do
      click_on taxon_b.name
      expect(current_path).to eq potepan_category_path(taxon_b.id)
      within '.side-nav' do
        expect(page).to have_content 'Categories'
        expect(page).to have_content taxon_b.name
      end
    end
  end
end
