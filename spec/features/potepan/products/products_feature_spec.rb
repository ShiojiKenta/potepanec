require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  feature "商品詳細ページを見ることができる" do
    given(:taxonomy)          { create(:taxonomy, name: 'Categories') }
    given(:taxon)             { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    given!(:product)          { create(:product, taxons: [taxon]) }
    given!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    background do
      visit potepan_product_path(product.id)
    end

    scenario "商品詳細ページで表示されているもの" do
      expect(page).to have_title product.name
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
      expect(page).to have_link 'Home', href: potepan_index_path
      expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
    end

    scenario "関連商品の商品名と価格が表示されている" do
      related_products.each do |related_product|
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
      end
    end

    scenario "関連商品をクリック時、商品詳細ページへ移動する" do
      related_products.each do |related_product|
        click_on related_product.name
        expect(page).to have_current_path potepan_product_path related_product.id
      end
    end

    context '関連商品が4個より多く生成された場合' do
      given!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

      scenario "関連する商品が４つ表示されている" do
        expect(page).to have_selector '.productBox', count: 4
      end
    end
  end
end
