require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "カテゴリーページ" do
    let!(:taxonomy)   { create(:taxonomy, name: 'Categories') }
    let(:taxon)       { create(:taxon) }
    let!(:product)    { create(:product, taxons: [taxon]) }
    let(:taxonomy_b)  { create(:taxonomy) }
    let(:taxon_b)     { create(:taxon, parent: taxonomy_b.root) }
    let(:product_b)   { create(:product, taxons: [taxon_b]) }

    before do
      get potepan_category_path(taxon.id)
    end

    describe "カテゴリーページ全体について" do
      it '200レスポンスが返ってきていること' do
        expect(response).to have_http_status '200'
      end

      it '商品名が含まれていること' do
        expect(response.body).to include product.name
      end

      it '商品価格が含まれていること' do
        expect(response.body).to include product.display_price.to_s
      end

      it 'カテゴリーに含まれない商品名が表示されていないこと' do
        expect(response.body).not_to include product_b.name
      end
    end

    describe "サイドバーについて" do
      it 'カテゴリー名が含まれていること' do
        expect(response.body).to include taxonomy.name
      end

      it '商品名が含まれていること' do
        expect(response.body).to include taxon.name
      end

      it '商品の数が含まれていること' do
        expect(response.body).to include taxon.products.count.to_s
      end
    end
  end
end
