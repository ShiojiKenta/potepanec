require 'rails_helper'

RSpec.describe 'Potepan::ProductDecorator', type: :model do
  describe 'related_products_test' do
    subject { product.taxons_relation_products }

    let(:taxonomy)           { create(:taxonomy) }
    let(:taxon1)             { create(:taxon, taxonomy: taxonomy) }
    let(:taxon2)             { create(:taxon, taxonomy: taxonomy) }
    let(:taxon3)             { create(:taxon, taxonomy: taxonomy) }
    let(:taxon4)             { create(:taxon, taxonomy: taxonomy) }
    let!(:product)           { create(:product, taxons: [taxon1, taxon2, taxon3, taxon4]) }
    let!(:related_products)  { create_list(:product, 4, taxons: [taxon1, taxon2, taxon3, taxon4]) }
    let(:other_taxon)        { create(:taxon) }
    let!(:unrelated_product) { create(:product, taxons: [other_taxon]) }

    it '関連商品が正常に表示されていること' do
      is_expected.to eq related_products
    end

    it '関連しない商品が含まれていないこと' do
      is_expected.not_to include unrelated_product
    end

    it '関連商品には、選択している商品自身が含まれないこと' do
      is_expected.not_to include product
    end

    it '関連商品が重複していないこと' do
      is_expected.to eq subject.distinct
    end
  end
end
