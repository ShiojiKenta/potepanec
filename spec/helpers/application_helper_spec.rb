require 'spec_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'title helper' do
    subject { full_title(page_title) }

    context "page_titleが空白の場合" do
      let(:page_title) { "" }

      it { is_expected.to eq("BIGBAG Store") }
    end

    context "page_titleが存在する場合" do
      let(:page_title) { "BASE_TITLE" }

      it { is_expected.to eq("BASE_TITLE | BIGBAG Store") }
    end

    context "page_titleがnilの場合" do
      let(:page_title) { nil }

      it { is_expected.to eq("BIGBAG Store") }
    end
  end
end
