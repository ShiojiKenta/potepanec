module Potepan::ProductDecorator
  def taxons_relation_products
    Spree::Product.in_taxons(taxons).distinct.where.not(id: id)
  end

  Spree::Product.prepend self
end
