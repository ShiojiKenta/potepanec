class Potepan::ProductsController < ApplicationController
  MAX_PRODUCT_SQL = 10
  MAX_RELATED_PRODUCT = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.taxons_relation_products.
      includes(master: [:default_price, :images]).
      limit(MAX_PRODUCT_SQL).
      sample(MAX_RELATED_PRODUCT)
  end
end
